/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly SLIDEV_TITLE?: string;
  readonly SLIDEV_SUBTITLE?: string;
  readonly SLIDEV_DATE?: string;

  readonly SLIDEV_AUTHOR_NAME?: string;
  readonly SLIDEV_AUTHOR_EMAIL?: string;
  readonly SLIDEV_AUTHOR_DESCRIPTION?: string;

  readonly SLIDEV_AUTHOR2_NAME?: string;
  readonly SLIDEV_AUTHOR2_EMAIL?: string;
  readonly SLIDEV_AUTHOR2_DESCRIPTION?: string;

  readonly SLIDEV_AUTHOR3_NAME?: string;
  readonly SLIDEV_AUTHOR3_EMAIL?: string;
  readonly SLIDEV_AUTHOR3_DESCRIPTION?: string;

  readonly SLIDEV_SUPERVISOR_NAME?: string;
  readonly SLIDEV_SUPERVISOR_EMAIL?: string;
  readonly SLIDEV_SUPERVISOR_DESCRIPTION?: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
