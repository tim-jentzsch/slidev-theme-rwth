const env = import.meta.env;

export type User = {
  name: string;
  email?: string;
  description?: string;
};

export const TITLE: string | undefined = env.SLIDEV_TITLE;
export const SUBTITLE: string | undefined = env.SLIDEV_SUBTITLE;
export const DATE: string | undefined = env.SLIDEV_DATE;

export const AUTHOR: User | undefined = env.SLIDEV_AUTHOR_NAME
  ? {
      name: env.SLIDEV_AUTHOR_NAME,
      email: env.SLIDEV_AUTHOR_EMAIL,
      description: env.SLIDEV_AUTHOR_DESCRIPTION,
    }
  : undefined;

export const AUTHOR2: User | undefined = env.SLIDEV_AUTHOR2_NAME
  ? {
      name: env.SLIDEV_AUTHOR2_NAME,
      email: env.SLIDEV_AUTHOR2_EMAIL,
      description: env.SLIDEV_AUTHOR2_DESCRIPTION,
    }
  : undefined;

export const AUTHOR3: User | undefined = env.SLIDEV_AUTHOR3_NAME
  ? {
      name: env.SLIDEV_AUTHOR3_NAME,
      email: env.SLIDEV_AUTHOR3_EMAIL,
      description: env.SLIDEV_AUTHOR3_DESCRIPTION,
    }
  : undefined;

export const SUPERVISOR: User | undefined = env.SLIDEV_SUPERVISOR_NAME
  ? {
      name: env.SLIDEV_SUPERVISOR_NAME,
      email: env.SLIDEV_SUPERVISOR_EMAIL,
      description: env.SLIDEV_SUPERVISOR_DESCRIPTION,
    }
  : undefined;
