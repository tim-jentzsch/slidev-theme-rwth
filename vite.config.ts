import { defineConfig } from "vite";

export default defineConfig({
  envPrefix: "SLIDEV_",
});
